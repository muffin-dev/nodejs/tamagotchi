export class Tamagotchi {

  private _hp = 100;
  private _hunger = 20;
  private _thirst = 20;

  public update(): void {
    this._hp -= 0.01;
    this._hunger += 0.01;
    this._thirst += 0.01;
  }

}
